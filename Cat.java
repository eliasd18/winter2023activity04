public class Cat 
{
		private String catBreed;
		private int age;
		private String name;
		public String color;

public Cat(String name, String catBreed, int age) 
{
	this.catBreed = catBreed;
	this.age = age;
	this.name = name;
}
public String getCatBreed() 
{
	return this.catBreed;
}
public int getAge() 
{
	return this.age;
}
public String getName()
{
	return this.name;
}
public void setCatBreed(String newCatBreed) 
{
	this.catBreed = newCatBreed;
}
public void setName(String newName) 
{
	this.name = newName;
}
public void setAge(int newAge) 
{
	this.age = newAge;
}
public void identifyBreed() 
{
	System.out.println(name + "'s breed is " + this.catBreed + "!");
}
public void sayAge() 
{
	System.out.println("Hello " + this.name + "!" + " You are " + this.age + " years old.");
}
public String pussInBoots(String name) 
{
	if (color(name)) 
	{
		this.color = "red";
		return color;
	}
	else 
	{
		this.color = "green";
		return color;
	}
}
private boolean color(String name) 
{
	boolean isColorRed = name.equals("Puss");
	return isColorRed;
}

}
